#

https://soundfly.typeform.com/to/GK0qWB

> - What is your area of musical expertise?

`Audio Engineering`, `Music Production`, `Songwriting`

-----

> - How did you learn the above skills?

In elementary school, music class was mandatory.  I loved it.  There, in the third grade, I learned to sightread "Ode to Joy."  Soon after, for Christmas, I received my first keyboard, a Yamaha PSR-85, and a children's songbook.  After learning the book's compositions, I soon began writing my own pieces and performing miniature recitals for family and neighbors.

I also had a love of poetry, which my 6th grade teacher noticed, recommending me for Duke University's Young Writer's Camp.  

My interests in piano and poetry converged one summer vacation when my cousin (also a minor) started his own record label, signing me and a few other relatives as artists.  My parents wouldn't actually let me sign the contract, but that didn't stop me from recording for and distributing through his label.  We were kids using half-working stereo mics and 4-track tape recorders, recording in the boiler room (of all places!), then bouncing down over analog to a CD master.  No computer!Interestingly, his label is still around and distributed by Sony/The Orchard.

After returning home that summer, I read the manual to my Yamaha keyboard and began using its sequencer to make beats, which I also recorded myself rapping over.  I was 15.

During my senior year in high school, I was able to perform at a local North Carolina venue, legendary producer 9th Wonder spinning my beat!  Not only that, but Duke University's Hip-Hop radio station gave a few of my songs some spins, and also granted me an on-air interview.  

With those accomplishments under my belt, I applied and was accepted to NYU's Clive Davis Institute, where I studied subjects like Hip-Hop Production, Audio Engineering, and Music for 30-Second Formats.

During my studies, I began an internship which quickly led to assistant engineer roles at major studios around the city, like Jay-Z's "Roc the Mic" studio and Mark Ronson's "Allido" record label studio.  While I had the pleasure of meeting Jay-Z, Beyonce, and Mark Ronson, it is under the tutelage of their engineers that I learned the craft, even earning myself some major-label credits along the way.  While I didn't get credit for many of the projects I worked on (Blondie, J. Cole, Wale), I wear the credit I did receive (most notably on Rihanna's *Rated R* album) as a badge of honor.

My film experience began as a grip, then camera operator, for my church. has come from my time working in NYU's Film Department, renting gear and recording studios, and troubleshooting student issues.  Also during college, I mixed Nyle's viral one-take music video "Let the Beat Build," landing me a brief interview in music industry trade magazine *Sound on Sound.*

I also occasionally provide boom operator, sound design, and mixing-for-film services for independent filmmaker Deck of Cards Entertainment.


> - Tell us about a time you learned a new musical skill and how you approached learning it.
 
I'll never forget learning basic music theory.  A teenager, I was at church playing piano by ear one day when the worship leader asked me what key I was playing in.  I had no clue. He watched me play and told me I was playing in the key of A Minor.  All I had done was avoid pressing any black keys, but, in doing so, was following the rules of both the A Minor and C Major scales.  I had just learned how to identify key!

Then, he taught me how to transpose one key to another by counting intervals ("Root, whole step, half step," etc.), moving the root (my thumb) to any new note (the new key) and using the intervals I counted earlier to deduce where the rest of the notes lay. It was liberating!  Finally, I could play the black keys too!  The first thing I did was go home, write down all the major and minor scales, and begin practicing my fingering. 

> - Tell us about a time you helped someone else learn something and how you approached it.

I was teaching music production to a group of students at NJCU's Business Incubator, and I had one particular student who had never played piano, but wanted to make beats. A firm believer that anyone can play in the key of A Minor, I was able to teach him to do just that, and he made his first beat: drums, bass, melody, chords and all.  

To teach him, first, I explained to him what a triad was, and how to shape his hand to play one.  Next, I pulled up a piano sound, told him to imagine his hand was a stamp, and to practice stamping triads up and down the keyboard, but only playing the white keys.  In doing so, he played every chord in the A minor scale.  Finally, I placed his thumb on A, told him to center his playing around it, and pressed record.  That was all he needed!  Playing everything by ear, he finished the entire track within minutes, and now he knew how to play in the key of A minor.

> - Discuss a time you kept a person or group of people motivated and working towards a common goal.

 While teaching at BUMP Records in San Francisco, I found that the high-schoolers were very productive at their stations -- producers making beats over here, songwriters writing songs over there, engineers watching over my shoulder -- but the vocal booth went essentially untouched.  We were functioning more like a publishing company than a record label.  Noticing this, I was able to suggest a collaboration between two singer/songwriters, a producer, and a rapper -- all students in the program -- and they combined forces to record an amazing R&B song with a rap feature.  I mixed and mastered it for them, and they performed it at a local talent show.  Never have I been so proud of my students!
 
> - Optional: Upload a CV or artist's bio.

- **Resume:** https://resume.creddle.io/resume/bbk7th0xid1

> - Optional: Include any relevant links you'd like to share.
 
- **Official Credits:** https://www.discogs.com/artist/1653900-Mykael-Alexander

- **Sears Commercial (rap artist):** https://www.youtube.com/watch?v=NkTL3y8KJW4

> - Optional: Anything else we should know about you?

>> **Why I teach:**
>> During my studies at the Clive Davis Institute, I asked a guest lecturer what his optimal vocal chain was.  Not just his compression ratio, but the whole chain and philosophy behind it.  
>> His response stuck with me (paraphrasing):  
>> 
>> > I only give that information to my assistant.

>> Contrast that with my studio internship at the time.  I was working under an engineer, Hernan Santiago, for Def Jam artists and producers. This same engineer trained Rihanna's chief recording engineer, so I knew I was in good hands.  The first thing I learned was his preferred vocal chain, so that I could have it patched and ready before he arrived to the studio.
>> 
>> The irony of paying for college to not receive information, versus working for free for the best information, was not lost on me.

>> Soon, it became my job to train the new interns and choose my own assistant.  I developed a habit of explaining what I was doing, as I was doing it, in simple terms.  This habit served me well when I began teaching in a more official capacity.
>> 
>> It's self-gratifying, teaching.  Every time one of my students "gets it," I feel their excitement.  Knowing that the knowledge I possess comes from legends in this industry, I almost feel like I'm passing on some sacred, secret knowledge.  Considering my aforementioned experience with certain college lecturers, you could even say I am.
>> 
>> 

